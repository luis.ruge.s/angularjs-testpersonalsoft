app.factory('onServiceError', function(appConstants){
    return {
        errorController: function(err, callbackError){
            if(err.status == 404)
                callbackError(appConstants.errorCargandoEmpleados); 
            else if(err.status == 505)
                callbackError(appConstants.errorServidor); 
            else
                callbackError(err.statusText); 
        }
    }
});