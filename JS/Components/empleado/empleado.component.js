app.component('empleado', {
    templateUrl: './JS/Components/empleado/empleado.template.html',
    bindings: {
        modalInstance: "<",
        resolve: "<"
    },
    controller: function EmpleadoController($scope) {
      var ctrl = this;
      $scope.model = {};
    
      ctrl.$onInit = function() {
        $scope.model = ctrl.resolve.modalData;
      }

      $scope.closeModal = function(){
        ctrl.modalInstance.close();
      }
    }
  });
  
  