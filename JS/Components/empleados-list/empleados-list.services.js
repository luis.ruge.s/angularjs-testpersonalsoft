app.factory('empleadosListServices', function($http, appConstants, onServiceError){
    return {
        cargar: function(callbackSuccess, callbackError){
            return $http({method: 'GET', url: '../data.json'})
            .then(
                fm => { callbackSuccess(fm.data); },
                err => { onServiceError.errorController(err,callbackError); }
            );
        }
    }
});