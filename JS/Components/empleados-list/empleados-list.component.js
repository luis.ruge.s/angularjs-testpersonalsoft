app.component('empleadosList', {
  templateUrl: './JS/Components/empleados-list/empleados-list.template.html',
  controller: function EmpleadosListController($scope, empleadosListServices,  $uibModal) {
    $scope.coleccion = [];
    $scope.modelSearch = {};
    $scope.table = {};

    $scope.buscarEmpleados = function(){
      empleadosListServices.cargar(callbackBuscarEmpleados, callbackError);

    };

    $scope.verEmpleado = function(empleado){
      $uibModal.open({
         component: "empleado",
         resolve: {
           modalData: function() {
             console.log("desde verEmpleado:");
             console.log(empleado);
             return empleado;
           }
         }
       })
       .result.then(function(result) {
          console.info("I was closed, so do what I need to do myContent's  controller now.  Result was->");
          console.info(result);
       }, 
       function(reason) {
          console.info("I was dimissed, so do what I need to do myContent's controller now.  Reason was->" + reason);
       });
    }

    callbackBuscarEmpleados = function(data){
      model = $scope.modelSearch;
      result = data.filter( x=> 
          (model.id == undefined || model.id == "" || x.id == model.id) &&
          (model.nombre == undefined || model.nombre == "" || x.nombre.toUpperCase().includes(model.nombre.toUpperCase())) &&          
          (model.documento == undefined || model.documento == "" || x.documento == model.documento) &&
          (model.cargo == undefined || model.cargo == "" || x.cargo.toUpperCase().includes(model.cargo.toUpperCase()))
      );

      $scope.coleccion=result; 
    };

    callbackCargueEmpleados = function(data){
      $scope.coleccion=data; 
      //$scope.table = $("#TablaEmpleados").DataTable();
    };
    
    callbackError = function(message){
      alert(message);
    };

    empleadosListServices.cargar(callbackCargueEmpleados, callbackError);
  }
});

