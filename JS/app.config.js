app.config(['$routeProvider',function config($routeProvider) {
      $routeProvider.
        when('/VerEmpleados', {
          template: '<empleados-list></empleados-list>'
        }).
        when('/Empleado/:empleadoId', {
          template: '<empleado></empleado>'
        }).
        otherwise('/VerEmpleados');
    }
]);